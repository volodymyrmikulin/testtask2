#### Fibonacci numbers

Number are given. Implement a function that return number of n-sequence, where n is argument function. If given argument not a number - "Passed argument is not a number". If you well in JS you can do this task with recursion.

Exp:
- fibonacciNumbers(1) // 1
- fibonacciNumbers(6) // 8
- fibonacciNumbers(11) // 89
- fibonacciNumbers(25) // 75025
- fibonacciNumbers([]) // 'Passed argument is not a number'
- fibonacciNumbers("s") // 'Passed argument is not a number'

For run:

1. Clone project to a local repository
2. Run npm install
3. Implement function
4. Run npm test
<hr>